package com.geekhub.examples.lesson6.json;

import com.geekhub.examples.lesson6.json.adapters.JsonDataAdapter;
import com.geekhub.examples.lesson6.json.adapters.UseDataAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * JsonSerializer converts Java objects to JSON representation.
 *
 */
public class JsonSerializer {

    /**
     * simpleTypes contains java classes for which we should not make any deeper serialization and we should return object as is
     * and use toString() method to get it serialized representation
     */
    private static Set<Class> simpleTypes = new HashSet<Class>(Arrays.asList(
            JSONObject.class,
            JSONArray.class,
            String.class,
            Integer.class,
            Short.class,
            Long.class,
            Byte.class,
            Double.class,
            Float.class,
            Character.class,
            Boolean.class,
            int.class,
            short.class,
            long.class,
            byte.class,
            double.class,
            float.class,
            char.class,
            boolean.class
    ));

    /**
     * Main method to convert Java object to JSON. If type of the object is part of the simpleTypes object itself will be returned.
     * If object is null String value "null" will be returned.
     * @param o object to serialize.
     * @return JSON representation of the object.
     */
    public static Object serialize(Object o) {
        if (null == o) {
            return "null";
        }
        if (simpleTypes.contains(o.getClass())) {
            return o;
        } else {
            try {
                return toJsonObject(o);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * Converts Java object to JSON. Uses reflection to access object fields.
     * Uses JsonDataAdapter to serialize complex values. Ignores @Ignore annotated fields.
     * @param o object to serialize to JSON
     * @return JSON object.
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static JSONObject toJsonObject(Object o) throws Exception {
        JSONObject result = new JSONObject();
        Field[] fields = o.getClass().getDeclaredFields();

        for (Field field : fields){
            if(field.isAnnotationPresent(Ignore.class)){
                continue;
            }else if(field.isAnnotationPresent(UseDataAdapter.class)){
                UseDataAdapter annotation = field.getAnnotation(UseDataAdapter.class);
                result.put(field.getName(), annotation.value().newInstance().toJson(getValueForField(o, field)));
            }else {
                result.put(field.getName(), serialize(getValueForField(o, field)));
            }
        }

        return result;
    }

    private static Object getValueForField(Object object, Field field) throws Exception {
        return new PropertyDescriptor(field.getName(), object.getClass()).getReadMethod().invoke(object);
    }

    /*
     * Second possible implementation. Ignoring private fields accessibility.
     */
    private static JSONObject toJsonObject2(Object o) throws IllegalAccessException, InstantiationException {
        JSONObject result = new JSONObject();

        Field[] fields = o.getClass().getDeclaredFields();
        for (Field f : fields) {
            try {
                f.setAccessible(true);

                if (f.isAnnotationPresent(Ignore.class))
                    continue;

                if (!f.isAnnotationPresent(UseDataAdapter.class)) {
                    result.put(f.getName(), f.get(o));
                } else {
                    UseDataAdapter annotation = f.getAnnotation(UseDataAdapter.class);
                    Class c = annotation.value();
                    JsonDataAdapter dataAdapter = (JsonDataAdapter) c.newInstance();

                    result.put(f.getName(), dataAdapter.toJson(f.get(o)));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return result;
    }
}
