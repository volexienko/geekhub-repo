package com.geekhub.examples.lesson6.json.adapters;

import com.geekhub.examples.lesson6.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        JSONArray result = new JSONArray();
        for (Object element : c){
            result.put(JsonSerializer.serialize(element));
        }
        return result;
    }
}
