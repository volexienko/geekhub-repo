package com.geekhub.examples.lesson5.source;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        return new File(pathToSource).canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(pathToSource))) {
            String line = reader.readLine();
            while(null != line) {
                sb.append(line);
                line = reader.readLine();
            }
        }
        return sb.toString();
    }
}
